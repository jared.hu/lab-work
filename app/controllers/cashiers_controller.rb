class CashiersController < ApplicationController
    before_action :authenticate_admin! 
    def new
        @cashier = Cashier.new
    end
    
    def index
        @cashiers = Cashier.all
    end
    
    def edit
        @cashier = Cashier.find(params[:id])
        if @cashier.admin != current_admin
            redirect_to cashiers_path
        end
    end
    
    def update
        @cashier = Cashier.find(params[:id])
        @cashier.update(cashier_params)
        @cashier.save
        
        redirect_to cashier_path
    end
    
    def destroy
        @cashier = Cashier.find(params[:id])
        @cashier.destroy
        
        redirect_to cashiers_path
    end
    
    def create
      @cashier = Cashier.new(cashier_params)
      @cashier.admin = current_admin
      @cashier.inspect
      @cashier.save
    
        redirect_to @cashier
    end

    def show
        id = params[:id]
        @cashier = Cashier.find(id)
    end
    
    
    private
        def cashier_params
            params.require(:cashier).permit!
        end
end
