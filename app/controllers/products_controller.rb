class ProductsController < ApplicationController
    before_action :authenticate_admin! 
    def new
        @product = Product.new
    end
    
    def index
        @products = Product.all
    end
    
    def edit
        @product = Product.find(params[:id])
        if @product.admin != nil
            @product = Product.find(params[:id])
        end
    end
    
    def update
        @product = Product.find(params[:id])
        @product.update(product_params)
        @product.save
        
        redirect_to product_path
    end
    
    def destroy
        @product = Product.find(params[:id])
        @product.destroy
        
        redirect_to products_path
    end
    
    def create
      @product = Product.new(product_params)
      @product.admin = current_admin
      @product.inspect
      @product.save
    
        redirect_to @product
    end

    def show
        id = params[:id]
        @product = Product.find(id)
    end
    
    
    private
        def product_params
            params.require(:product).permit!
        end
end
