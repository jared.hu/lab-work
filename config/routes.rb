Rails.application.routes.draw do
  devise_for :admins
    resources :products
    resources :cashiers
  
  root 'cashiers#home'
  
end
